package info.androidhive.loginandregistration.app;

public class AppConfig {
	// Server user login url
	public static String URL_LOGIN = "http://rulta.com/relationships/login.php";

	// Server user register url
	public static String URL_REGISTER = "http://rulta.com/relationships/register.php";

	// Server user store location record
	public static String URL_LOC_REC = "http://rulta.com/relationships/add_location_record.php";

	// Server find relationships
	public static String URL_REL = "http://rulta.com/relationships/find_relationships.php";
}
